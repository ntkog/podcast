# Podcast 27-Feb-2020: FOSDEM 2020

## Asistentes

- [Francisco Picolini](https://twitter.com/francjp)
- [María Encinar](https://twitter.com/encinar)
- [J. Manrique López](https://twitter.com/jsmanrique) | @jsmanrique

## ¿Qué nos ha gustado de FOSDEM?

* Manrique
  * [Orbit model](https://github.com/orbit-love/orbit-model) de [Josh Dzielak](https://github.com/dzello)
  * [Espruino](https://www.espruino.com/) y [Bangle.js](https://www.espruino.com/Bangle.js)
* María
  * [Creciendo con contribuciones sostenibles a través de redes de embajadores](https://fosdem.org/2020/schedule/event/ambassadornetworks/)
  * [Sé la persona lider que necesitas en Open Source](https://fosdem.org/2020/schedule/event/leadeross/)
  * [Cauldron](https://cauldron.io)
* Fran
  * ¿¿??

## Los *takeaways* de hoy

* María
  * [Modernizando communidades - The Mozilla way](https://www.nukeador.com/23/01/2020/modernizing-communities-the-mozilla-way/)
  * [Developer Marketing](https://www.developer-marketing.com/)
  * [Devrel Collective](https://devrelcollective.fun/)
* Manrique
  * [Forja tu futuro con Open Source](https://pragprog.com/book/vbopens/forge-your-future-with-open-source)
  * [Mozilla & the Rebel Alliance](https://report.mozilla.community/)
  * [The Business Value of Developer Relations](https://www.apress.com/gp/book/9781484237472)
* Fran
  * [El panorama de Meetups y eventos en España](https://news.malt.com/wp-content/uploads/2019/11/malt-report-1.pdf) (**PDF**)
  * [Broaden your Community and Developer Relations reach through collaboration](https://dev.to/jerdog/broaden-your-community-and-developer-relations-reach-through-collaboration-413)