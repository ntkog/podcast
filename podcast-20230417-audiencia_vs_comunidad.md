# Podcast 17-Abril-2023: Audiencia vs Comunidad


## Asistentes

- David Gómez
- Diego Zapico Ferreiro
- Joaquin Engelmo Moriche (Kini)
- Jorge Barrachina


## Referencias

- [ley de hierro de la oligarquía](https://es.wikipedia.org/wiki/Ley_de_hierro_de_la_oligarqu%C3%ADa#:~:text=La%20%22ley%20de%20hierro%20de%20la%20oligarqu%C3%ADa%22%20establece%20que%20todas,verdadera%20democracia%2C%20especialmente%20en%20grupos)
- [comunidades de práctica](https://cfp.cervantes.es/recursos/proyectos/comunidades_de_practica.htm#:~:text=Una%20comunidad%20de%20pr%C3%A1ctica%20es,%2C%20en%20definitiva%2C%20a%20profesionalizarse)
- Estudio en [semantic scholar](https://www.semanticscholar.org/): 
 - [Why Stack Overflow Fails ?](https://www.semanticscholar.org/paper/Why-is-Stack-Overflow-Failing-Preserving-in-Srba-Bielikov%C3%A1/364e392c56232648d369418be96a52fbac67161b)
- [Orbit model](https://orbit.love/model)

- [Skin in the Game](https://www.goodreads.com/book/show/36064445-skin-in-the-game?from_search=true&from_srp=true&qid=YtdA87OfS1&rank=2)
- [Status versus Reputation as Motivation in Online Communities](https://scholarspace.manoa.hawaii.edu/server/api/core/bitstreams/189c192e-44bc-4539-bb15-b5c83f7d0afb/content)
- [Lurkers gonna lurk](https://twitter.com/DevRelPuzzle/status/1644260419865833472)

